/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample.Controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import com.jfoenix.controls.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rivera.daniel.bl.Item;

public class ControllerInventario implements Initializable {

    @FXML
    private DatePicker datePicker;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    //---------------------------------------------------------------------
    //BOTONES
    @FXML
    private JFXButton registrarB;
    //ComboBox
    @FXML
    private JFXComboBox<String> categoriaCB;
    @FXML
    private JFXComboBox<String> estadoCB;
    //---------------------------------------------------------------------
    //JFXTextField
    @FXML
    private JFXTextField nombreTF;
    @FXML
    private JFXTextField descripcionTF;
    //---------------------------------------------------------------------
    //JFXDatePicker
    @FXML
    private JFXDatePicker fechacompraDP;
    @FXML
    private JFXDatePicker antiguedadDP;

    //---------------------------------------------------------------------
    //DATEPICKER
    @FXML
    private DatePicker datep;
    //---------------------------------------------------------------------
    //AnchorPane
    @FXML
    private AnchorPane headerAP;
    //---------------------------------------------------------------------
    //TableView
    @FXML
    private TableView<rivera.daniel.bl.Item> itemsTV;
    //---------------------------------------------------------------------
    //TableColumn
    @FXML
    private TableColumn colNombre;
    @FXML
    private TableColumn colDescripcion;
    @FXML
    private TableColumn colEstado;
    @FXML
    private TableColumn colCategoria;
    @FXML
    private TableColumn colFechaCompra;
    @FXML
    private TableColumn colAntiguedad;
    //---------------------------------------------------------------------
    //OBSERVABLE LIST
    @FXML
    private ObservableList<Item> Items;

    //---------------------------------------------------------------------
    //COMBOBOX
    @FXML
    private JFXComboBox<String> estadoCP;
    @FXML
    private JFXComboBox<String> categoriaCP;
    //---------------------------------------------------------------------
    //LISTAS
    //Creamos las Listas
    ObservableList<String> estadoContent = FXCollections.observableArrayList(
            "Nuevo",
            "Usado",
            "Antiguo",
            "Sin Abrir"
    );
    //---------------------------------------------------------------------
    ObservableList<String> categoriaContent = FXCollections.observableArrayList(
            "Por",
            "Agregar"
    );

    //---------------------------------------------------------------------
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Items = FXCollections.observableArrayList();
        this.itemsTV.setItems(Items);
        estadoCB.setItems(estadoContent);
        categoriaCB.setItems(categoriaContent);
        this.colNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.colDescripcion.setCellValueFactory(new PropertyValueFactory("descripcion"));
        this.colEstado.setCellValueFactory(new PropertyValueFactory("estado"));
        this.colCategoria.setCellValueFactory(new PropertyValueFactory("categoria"));
        // this.colFechaCompra.setCellValueFactory(new PropertyValueFactory("Fecha"));
        // this.colAntiguedad.setCellValueFactory(new PropertyValueFactory("antiguedad"));

    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @FXML
  /*  private void OnAgregarButtonClicked(ActionEvent event) {
      //  try{
       String nombre = this.nombreTF.getText();
       String descripcion = this.descripcionTF.getText();
       String estado = this.estadoCB.getValue().toString();
        String categoria = this.categoriaCB.getValue().toString();
        //String value = this.datep.getValue().toString();
         //int edad = Integer.parseInt(edad)this.txtNombre.getText();   //EN PROYECTO

             Item itm = new Item(nombre,descripcion,estado,categoria);

             if(!this.Items.contains(itm)){     //SINO CONTIENE EL ITEM, ENTONCES LO AGREGA
                 this.Items.add(itm);    //ACA LO AGREGA
                 this.itemsTV.refresh(); //REFRESCA
             }else{   //SINO MANDA ALERTA
                 Alert alert = new Alert (Alert.AlertType.ERROR);
                 alert.setHeaderText(null);
                 alert.setTitle("Error");
                 alert.setContentText("El Item ya existe.");
                 alert.showAndWait();
             }
    }       */
//*******************************************************************
    public String toString(LocalDate t) {
        if (t != null) {
            return formatter.format(t);
        }
        return null;
    }

    //*******************************************************************
    public LocalDate fromString(String string) {
        if (string != null && !string.trim().isEmpty()) {
            return LocalDate.parse(string, formatter);
        }
        return null;
    }

    //*******************************************************************
    @FXML
    private void Modificar(ActionEvent event) {

        // Obtengo el item seleccionado
        Item p = this.itemsTV.getSelectionModel().getSelectedItem();

        // Si el item es  nulo, lanzo error
        if (p == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debes seleccionar un Item");
            alert.showAndWait();

        } else {


            // Obtengo los datos del formulario
            String nombre = this.nombreTF.getText();
            String descripcion = this.descripcionTF.getText();
            String estado = this.estadoCB.getValue().toString();
            String categoria = this.categoriaCB.getValue().toString();

            // Creo un Item
            Item aux = new Item(nombre, descripcion, estado, categoria);

            // Compruebo si el item esta en el lista
            if (!this.Items.contains(aux)) {

                // Modifico el objeto
                p.setNombre(aux.getNombre());
                p.setDescripcion(aux.getDescripcion());
                p.setEstado(aux.getEstado());
                p.setCategoria(aux.getCategoria());

                // Refresco la tabla
                this.itemsTV.refresh();

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setTitle("Info");
                alert.setContentText("Item modificado");
                alert.showAndWait();

            } else {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("El Item no existe!");
                alert.showAndWait();
            }

        }

    }

    //*********************************************************************************
//*********************************************************************************
    @FXML
    private void Seleccionar(MouseEvent event) {
        // Obtengo la persona seleccionada
        Item p = this.itemsTV.getSelectionModel().getSelectedItem();
        // Sino es nula seteo los campos
        if (p != null) {
            this.nombreTF.setText(p.getNombre());
            this.descripcionTF.setText(p.getDescripcion());
            this.estadoCB.setValue(p.getEstado());
            this.categoriaCB.setValue(p.getCategoria());
        }

    }//FIN SELECCIONAR

    //*********************************************************************************
    @FXML
    private void Eliminar(ActionEvent event) {
        Item itm = this.itemsTV.getSelectionModel().getSelectedItem();   //EL ITM SE CONVIERTE EN LO SELECCIONADO
        if (itm == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);                                //SINO SELECCIONA NINGUNO DICE ESTO
            alert.setTitle("Error");
            alert.setContentText("Debes seleccionar un Item!");
            alert.showAndWait();
        } else {
            this.Items.remove(itm);   //ELIMINA EL OBJETO ESCOGIDO
            this.itemsTV.refresh();
        }
    }
//*********************************************************************************

    @FXML
    private void agregarItem(ActionEvent event) {

        try {
            // Cargo la vista
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/AddVentana.fxml"));

            // Cargo la ventana
            Parent root = loader.load();

            // Asigno el controlador
            ControllerVentana controlador = loader.getController();
            controlador.inicializadorAtributos(Items); //INICIALIZA LA COLECCIÓN PARA QUE NO SEA NULA

            // Creo el Scene
            Scene scene = new Scene(root);   //CREO UNA ESCENA Y LE ASIGNO EL PADRE
            Stage stage = new Stage();   //CREO UNA STAGE
            stage.initModality(Modality.APPLICATION_MODAL); //Modalidad para que no vuelva a la ventana anterior
            stage.setScene(scene); //ASIGNAMOS LA ESCENA
            stage.showAndWait();


            Item itm = controlador.getObjeto();  // Agarro la persona devuelta CATCH!!!!
            if (itm != null) {

                // Añado la persona
                this.Items.add(itm);

                // Refresco la tabla
                this.itemsTV.refresh();
            }

        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(ex.getMessage());
            alert.showAndWait();

        }

    }
    //---------------------------------------------------------------------

    @FXML
    private void modificarItem(ActionEvent event) {

        Item p = this.itemsTV.getSelectionModel().getSelectedItem();

        if (p == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debes seleccionar un Item");
            alert.showAndWait();
        } else {

            try {
                // Cargo la vista
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/AddVentana.fxml"));

                // Cargo la ventana
                Parent root = loader.load();

                // Cojo el controlador
                ControllerVentana controlador = loader.getController();
                controlador.initAttributtes(Items, p);

                // Creo el Scene
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();

                // cojo la persona devuelta
                Item aux = controlador.getObjeto();
                if (aux != null) {
                    this.itemsTV.refresh();
                }

            } catch (IOException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText(ex.getMessage());
                alert.showAndWait();
            }
        }

    }
//---------------------------------------------------------------------
    @FXML
    private void eliminarItem(ActionEvent event) {

        Item p = this.itemsTV.getSelectionModel().getSelectedItem();

        if (p == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debes seleccionar un Item");
            alert.showAndWait();
        } else {
            // Elimino la persona
            this.Items.remove(p);
            this.itemsTV.refresh();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Info");
            alert.setContentText("Se ha borrado el Item");
            alert.showAndWait();

        }
    }
}