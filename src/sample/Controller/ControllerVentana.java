package sample.Controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import rivera.daniel.bl.Item;


public class ControllerVentana implements Initializable {

    @FXML
    private JFXTextField nombreTF;

    @FXML
    private JFXTextField descripcionTF;

    @FXML
    private JFXComboBox<String> estadoCB;

    @FXML
    private JFXComboBox<String> categoriaCB;

    @FXML
    private Button guardarB;

    @FXML
    private Button salirB;

    private Item objeto;

    //PARA CONTROLAR LA EXISTENCIA DEL MISMO
    private ObservableList<Item> items;


    //LISTAS
    //Creamos las Listas
    ObservableList<String> estadoContent = FXCollections.observableArrayList(
            "Nuevo",
            "Usado",
            "Antiguo",
            "Sin Abrir"
    );
    //---------------------------------------------------------------------
    ObservableList<String> categoriaContent = FXCollections.observableArrayList(
            "Por",
            "Agregar"
    );
//---------------------------------------------------------------------

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        estadoCB.setItems(estadoContent);
        categoriaCB.setItems(categoriaContent);
    }

//INICIALIZO PARA AGREGAR
    public void inicializadorAtributos(ObservableList<Item> items){
        this.items=items;
    }

    // INICIALIZO PARA MODIFICAR
    public void initAttributtes(ObservableList<Item> itm, Item p) {
        this.items = itm;
        this.objeto = p;
        // cargo los datos de item
        this.nombreTF.setText(p.getNombre());
        this.descripcionTF.setText(p.getDescripcion());
        this.estadoCB.setValue(p.getEstado());
        this.categoriaCB.setValue(p.getCategoria());
    }

    //*******************************************************************
    @FXML
    private void guardar(ActionEvent event) {

        // Cojo los datos
     String nombre = this.nombreTF.getText();
     String descripcion = this.descripcionTF.getText();
     String estado = this.estadoCB.getValue();
     String categoria = this.categoriaCB.getValue();
        // Creo la persona
        Item p = new Item(nombre, descripcion, estado, categoria);

        // Compruebo si la persona existe
        if (!items.contains(p)) {

            // Modificar
            if (this.objeto != null) {

                // Modifico el objeto
                this.objeto.setNombre(nombre);
                this.objeto.setDescripcion(descripcion);
                this.objeto.setEstado(estado);
                this.objeto.setCategoria(categoria);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setTitle("Informacion");
                alert.setContentText("Se ha modificado correctamente");
                alert.showAndWait();

            } else {
                // insertando

                this.objeto = p;
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setTitle("Informacion");
                alert.setContentText("Se ha añadido correctamente");
                alert.showAndWait();

            }
            // Cerrar la ventana
            Stage stage = (Stage) this.guardarB.getScene().getWindow();
            stage.close();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("La persona ya existe");
            alert.showAndWait();
        }

    }
//---------------------------------------------------------------------
    @FXML private void salir(ActionEvent event){
        this.objeto = null;
        //CERRAR LA VENTANA
        Stage stage = (Stage) this.salirB.getScene().getWindow();
        stage.close();
    }

    public Item getObjeto() {   //PARA DEVOLVERLE EL ITEM A INVENTARIO
        return objeto;
    }
}

